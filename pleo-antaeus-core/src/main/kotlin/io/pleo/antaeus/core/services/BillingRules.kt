package io.pleo.antaeus.core.services

import io.pleo.antaeus.core.services.billing_rules.CustomerCurrencyMatches
import io.pleo.antaeus.core.services.billing_rules.Rule
import io.pleo.antaeus.models.Customer
import io.pleo.antaeus.models.Invoice

class BillingRules(private val ruleList:Array<Rule> = arrayOf(CustomerCurrencyMatches())) {

    fun canCharge(invoice: Invoice, customer: Customer) : Boolean {
        this.ruleList.forEach {
            val ruleResult = it.satisfied(invoice, customer)
            if (!ruleResult){
                return false
            }
        }

        return true
    }
}