package io.pleo.antaeus.core.external.payment_providers

import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import io.pleo.antaeus.core.exceptions.CurrencyMismatchException
import io.pleo.antaeus.core.exceptions.CustomerNotFoundException
import io.pleo.antaeus.models.Currency
import io.pleo.antaeus.models.Customer
import io.pleo.antaeus.models.Money
import org.junit.jupiter.api.Assertions.*
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import java.math.BigDecimal

object StripeSpec: Spek({
    describe("Stripe") {
        val subject by memoized { spyk<Stripe>(recordPrivateCalls = true) }
        val customer by memoized { mockk<Customer>(relaxed = true) }
        val amount = Money(BigDecimal(10), Currency.EUR)
        it("returns false if customer balance is less than the charged amount"){
            //i would never mock private methods but in the context of the exercise I am seeing it as mocking a http call, or some other api.
            every { subject["customerBalance"](ofType(Customer::class)) } returns BigDecimal(9)
            assertFalse(subject.charge(customer, amount))
        }

        it("returns true if customer balance is eq to the charged amount"){
            every { subject["customerBalance"](ofType(Customer::class)) } returns BigDecimal(10)
            assertTrue(subject.charge(customer, amount))
        }

        it("returns false when customer is not found"){
            every { subject["customerBalance"](ofType(Customer::class)) } returns BigDecimal(10)
            every { subject["process"]() } throws CustomerNotFoundException(1)
            assertFalse(subject.charge(customer, amount))
        }

        it("returns false when customer is not found"){
            every { subject["customerBalance"](ofType(Customer::class)) } returns BigDecimal(10)
            every { subject["process"]() } throws CurrencyMismatchException(1,1)
            assertFalse(subject.charge(customer, amount))
        }
    }
})