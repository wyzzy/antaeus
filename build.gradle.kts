import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val kotlin_version = "1.3.70"
val spek_version = "2.0.15"

plugins {
    base
    kotlin("jvm") version "1.3.70"
}

allprojects {
    group = "io.pleo"
    version = "1.0"
    apply(plugin = "java")

    repositories {
        mavenCentral()
        jcenter()
    }

    tasks.withType<KotlinCompile>().configureEach {
        kotlinOptions.jvmTarget = "11"
    }

    dependencies {
        testImplementation("org.spekframework.spek2:spek-dsl-jvm:$spek_version")
        testRuntimeOnly("org.spekframework.spek2:spek-runner-junit5:$spek_version")

        // spek requires kotlin-reflect, can be omitted if already in the classpath
        testRuntimeOnly("org.jetbrains.kotlin:kotlin-reflect:$kotlin_version")
    }

    tasks.withType<Test> {
        useJUnitPlatform {
            includeEngines("spek2")
        }
    }
}
