package io.pleo.antaeus.core.services.billing_rules

import io.mockk.mockk
import io.pleo.antaeus.models.*
import org.junit.jupiter.api.Assertions.*
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import java.math.BigDecimal

object CustomerCurrencyMatcheSpec: Spek({
    describe("CustomerCurrencyMatches") {
        val subject = CustomerCurrencyMatches()

        describe("#satisfied") {
            context("invoice currency matches customer currency") {
                val invoice = Invoice(1, 1, Money(BigDecimal.valueOf(1), Currency.EUR), InvoiceStatus.PENDING)
                val customer = Customer(1, Currency.EUR)

                it("returns true") {
                    assertTrue(subject.satisfied(invoice, customer))
                }
            }

            context("invoice currency doesn't match customer currency") {
                val invoice = Invoice(1, 1, Money(BigDecimal.valueOf(1), Currency.EUR), InvoiceStatus.PENDING)
                val customer = Customer(1, Currency.GBP)

                it("returns true") {
                    assertFalse(subject.satisfied(invoice, customer))
                }
            }
        }
    }
})