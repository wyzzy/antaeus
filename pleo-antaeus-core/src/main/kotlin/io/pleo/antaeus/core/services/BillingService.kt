package io.pleo.antaeus.core.services

import io.pleo.antaeus.core.external.PaymentProvider
import io.pleo.antaeus.data.AntaeusDal
import io.pleo.antaeus.models.Customer
import io.pleo.antaeus.models.Invoice

class BillingService(
    private val customerService: CustomerService,
    private val invoiceService: InvoiceService,
    private val paymentProvider: PaymentProvider,
    private val billingRules: BillingRules = BillingRules()
) {
    fun call(invoice: Invoice): Boolean {
        if (canCharge(invoice)){
            charge(invoice)
            return true
        }
        return false
    }

    private fun canCharge(invoice: Invoice): Boolean{
        val customer = customerService.fetch(invoice.customerId)
        return billingRules.canCharge(invoice, customer)
    }

    private fun charge(invoice: Invoice): Invoice{
        val customer = customerService.fetch(invoice.customerId)
        paymentProvider.charge(customer, invoice.amount)
        return invoice
    }
}
