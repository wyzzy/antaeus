package io.pleo.antaeus.core.services

import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.pleo.antaeus.core.external.PaymentProvider
import io.pleo.antaeus.data.AntaeusDal
import io.pleo.antaeus.models.Customer
import io.pleo.antaeus.models.Invoice
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

object BillingServiceSpec: Spek({
    afterEachTest { clearAllMocks() }

    describe("BillingService") {

        val paymentProvider by memoized { mockk<PaymentProvider>(relaxed = true) }
        val customerService by memoized { mockk<CustomerService>(relaxed = true) }
        val invoiceService by memoized { mockk<InvoiceService>(relaxed = true) }
        val billingRules by memoized {
            mockk<BillingRules> {
                every { canCharge(ofType(Invoice::class), ofType(Customer::class)) } returns true
            }
        }
        val subject by memoized { BillingService(
            paymentProvider = paymentProvider,
            billingRules = billingRules,
            customerService = customerService,
            invoiceService = invoiceService
        ) }

        describe("#call") {
            val invoice = mockk<Invoice>(relaxed = true)

            it("takes money for an invoice") {
                assertTrue(subject.call(invoice))
            }

            it("calls the payment provider to collect the charge"){
                subject.call(invoice)

                verify(exactly = 1) { paymentProvider.charge(ofType(Customer::class), any()) }
            }
        }
    }
})