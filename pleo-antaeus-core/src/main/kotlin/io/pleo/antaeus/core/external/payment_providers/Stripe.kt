package io.pleo.antaeus.core.external.payment_providers
import io.pleo.antaeus.core.exceptions.CurrencyMismatchException
import io.pleo.antaeus.core.exceptions.CustomerNotFoundException
import io.pleo.antaeus.core.exceptions.NetworkException
import io.pleo.antaeus.core.external.PaymentProvider
import io.pleo.antaeus.models.Customer
import io.pleo.antaeus.models.Money
import java.math.BigDecimal

class Stripe : PaymentProvider {
    override fun charge(customer: Customer, amount: Money): Boolean {
        val customerBalance = customerBalance(customer)
        if (amount.value > customerBalance){
            return false
        }
        return processPayment(customer, amount)
    }

    private fun processPayment(customer: Customer, amount: Money): Boolean{
        try{
            // call some external API to charge the customer and get the result of processing the payment
            // i'll fake it here by calling a private method
            process()
        }catch (ex: Exception) {
            when(ex) {
                is CustomerNotFoundException, is CurrencyMismatchException -> {
                    // possibly send to an Error tracking tool
                    return false
                }
                is NetworkException -> {
                    //maybe retry
                }
                else -> throw ex
            }
        }

        return true
    }

    private fun customerBalance(customer: Customer) : BigDecimal {
        // call API to get the balance
        // to be honest i don't see the point, we can just catch that in processPayment
        return BigDecimal(1)
    }

    private fun process(){
        // noop
    }
}