package io.pleo.antaeus.core.services.billing_rules

import io.pleo.antaeus.models.Customer
import io.pleo.antaeus.models.Invoice

interface Rule {
    fun satisfied(invoice: Invoice, customer: Customer): Boolean
}