## The architecture

I have drawn the following possible architecture diagram:

![arch](Anateus.png)

The app would have 2 contexts (among others yet to be identified).

* The 3-tier arch domain (already existing)
* The payment processing domain (added in the diagram above)

These contexts are quite separate and could possibly be split into two separate services

## Processing of invoices

The Invoices need to be processed once a month.

### PaymentOrchestrator

We can use CRON to run a scheduled task on the agreed day. This task can spawn multiple async jobs (one per invoice?) to be processed in parallel. (These jobs can have a retry mechanism)

The orchestrator could figure out which Invoices would need processing.

*we can have logic of which kind of invoices we want to process eg. by due date (see further considerations below)


### Billing Service

Each job can call the billing service to process an invoice.

* The service would decide based on some rules (`RulesList`) if the invoice is in a state to be processed from the perspective of our system.
* If yes, then would call payment provider to process
* If no, then possibly move the invoice to a different state.

### PaymentProvider

This is where we connect the app to an external payment service to take the payment.

I would think there could be a provider per region, eg EMEA, AMERICAS, etc

The API integration could raise exceptions outside of our APP control or rules list, in which case we could act accordingly
* eg. log, retry in place, raise and let the `PaymentOrchestrator` job retry later.


## Further considerations

* We could add a state machine for invoices
* Multiple orchestrators for different states of invoices
* Adding logging and/or error reporting.
* Customer balance if ambiguous on which system it exists. From the body of the exercise it appears is not on this system.
    * What is a balance anyway, can't we just use their card details?
    

* We could **split the processor in a microservice** and use the REST API of Antaeus to expose `Invoice` and `Customer` data. (See diagram below)

![arch2](AnateusServices.png)

## Things I lacked

* Knowing how to efficiently mock things
* I prefer RSpec style of test syntax rather than Junit
* Knowing when to stop on this challenge.
