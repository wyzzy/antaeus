package io.pleo.antaeus.core.services

import io.mockk.every
import io.mockk.mockk
import io.pleo.antaeus.core.services.billing_rules.CustomerCurrencyMatches
import io.pleo.antaeus.core.services.billing_rules.Rule
import io.pleo.antaeus.models.Customer
import io.pleo.antaeus.models.Invoice
import org.junit.jupiter.api.Assertions.*
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe

object BillingRulesSpec: Spek({
    describe("BillingRules") {
        val subject = BillingRules()

        describe("#canCharge") {
            val invoice = mockk<Invoice>(relaxed = true)
            val customer = mockk<Customer>(relaxed = true)

            it("returns true if all rules are satisfied") {
                assertTrue(subject.canCharge(invoice, customer))
            }

            it("returns false if a rule is not satisfied") {
                val customerCurrencyMatchesRule = mockk<CustomerCurrencyMatches>{
                    every { satisfied(ofType(Invoice::class), ofType(Customer::class)) } returns false
                }
                val rules: Array<Rule> = arrayOf(customerCurrencyMatchesRule)

                assertFalse(BillingRules(ruleList = rules).canCharge(invoice, customer))
            }
        }
    }
})