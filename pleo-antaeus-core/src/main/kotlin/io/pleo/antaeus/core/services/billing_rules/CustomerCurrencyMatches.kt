package io.pleo.antaeus.core.services.billing_rules

import io.pleo.antaeus.models.Customer
import io.pleo.antaeus.models.Invoice

class CustomerCurrencyMatches : Rule {
    override fun satisfied(invoice: Invoice, customer: Customer): Boolean {
        return invoice.amount.currency == customer.currency
    }
}